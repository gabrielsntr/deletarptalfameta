﻿using DeletaRPTAlfameta.Model;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Button = System.Windows.Controls.Button;

namespace DeletaRPTAlfameta
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        List<Relatorio> relatorios = new List<Relatorio>();
        List<Rpt> arquivos = new List<Rpt>();
        String schema;

        public MainWindow()
        {

            InitializeComponent();
            entryCaminho.Text = "C:\\Alfameta\\Relatorios\\";
            try {
                List<String> schemas = new List<String>();
                schemas = Controller.Schemas.getSchemas();
                foreach (String schema in schemas) {
                    comboSchemas.Items.Add(schema);
                }
            } catch (MySql.Data.MySqlClient.MySqlException ex) {
                System.Windows.MessageBox.Show("Erro no MySQL: " + ex.ToString(), "Erro", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void ObtemRelatorios() {
            try {
                schema = comboSchemas.SelectedValue.ToString();
                relatorios = Controller.Relatorios.GetRelatorios(schema);
                String caminho = entryCaminho.Text;
                arquivos = Controller.Rpts.GetRpts(caminho);
                Controller.Rpts.SetCadastrado(arquivos, relatorios);
                Controller.Relatorios.VerificaModuloAtivo(relatorios);
                dataGridRelatoriosSistema.ItemsSource = relatorios;
                dataGridRelatoriosPasta.ItemsSource = arquivos;
                int qtdRptSemCadastro = 0;
                int qtdRelModulosInativos = 0;
                foreach(Rpt rpt in arquivos) {
                    if (rpt.Cadastrado == "N") {
                        qtdRptSemCadastro += 1;
                    }
                }
                foreach (Relatorio r in relatorios) {
                    if (r.ModuloAtivo == "N") {
                        qtdRelModulosInativos += 1;
                    }
                }
                lblQtdSistema.Content = relatorios.Count-qtdRelModulosInativos + "/" + relatorios.Count;
                lblQtdPasta.Content = arquivos.Count-qtdRptSemCadastro + "/" + arquivos.Count;
            } catch (NullReferenceException) {
                System.Windows.MessageBox.Show("Escolha o dicionario e caminho a ser analisado.", "Atenção", MessageBoxButton.OK, MessageBoxImage.Warning);

            } catch (DirectoryNotFoundException) {
                System.Windows.MessageBox.Show("Não foi possível localizar o diretório especificado.", "Erro", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ExcluiArquivos() {
            List<Rpt> listaExclusao = Controller.Rpts.ListaRptExclusao(arquivos);
            if (listaExclusao.Count > 0 && System.Windows.MessageBox.Show("Confirma a exclusão de " + listaExclusao.Count + " arquivos?", "Atenção", MessageBoxButton.YesNo, MessageBoxImage.Warning) == System.Windows.MessageBoxResult.Yes) {
                int[] result = Controller.Rpts.ExcluiRpt(listaExclusao);
                String msg = "Foram removidos " + result[0] + " arquivos.";
                if (result[1] > 0) {
                    msg += " Não foi possível remover " + result[1] + " arquivos.";
                }
                System.Windows.MessageBox.Show(msg, "Concluído!", MessageBoxButton.OK, MessageBoxImage.Information);
                ObtemRelatorios();
            } else if (listaExclusao.Count == 0) {
                System.Windows.MessageBox.Show("Não há arquivos .rpt não cadastrados para serem excluídos.", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void ExcluiModulos() {
            List<String> modulos = Controller.Conexao.ListaModulosInativos(schema);
            String modulosString = "";
            int i = 0;
            foreach (String modulo in modulos) {
                if (i == 0) {
                    modulosString += "\n" + modulo;
                    i++;
                } else {
                    modulosString += "\n" + modulo;
                    i++;
                }
            }
            if (modulos.Count > 0 && (System.Windows.MessageBox.Show("Confirma a exclusão de " + modulos.Count + " módulos inativos? Módulos: " + modulosString, "Atenção", MessageBoxButton.YesNo, MessageBoxImage.Warning) == System.Windows.MessageBoxResult.Yes)) {
                try {
                    int rows = Controller.Conexao.DeletaModulosInativos(schema);
                    System.Windows.MessageBox.Show("Foram excluídos " + rows + " registros de " + modulos.Count + " módulos.", "Concluído", MessageBoxButton.OK, MessageBoxImage.Information);

                } catch (Exception ex) {
                    System.Windows.MessageBox.Show("Não foi possível excluir os módulos. Mensagem original: " + ex.ToString(), "Erro", MessageBoxButton.OK, MessageBoxImage.Error);
                } 
            } else if (modulos.Count == 0) {
                System.Windows.MessageBox.Show("Não há módulos inativos para serem excluídos.", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            ObtemRelatorios();
        }

        private void Button_Click(object sender, RoutedEventArgs e) {
            /*****Botão******/
            Button botao = (Button)sender;
            if (botao.Name == "btnAbrirPasta") {
                CommonOpenFileDialog folderDialog = new CommonOpenFileDialog() ;
                folderDialog.InitialDirectory = "C:\\";
                folderDialog.IsFolderPicker = true;
                //CommonFileDialogResult result = folderDialog.ShowDialog();
                if (folderDialog.ShowDialog() == CommonFileDialogResult.Ok) {
                    entryCaminho.Text = folderDialog.FileName + "\\";
                }
            }
            if (botao.Name == "btnObterRelatorios") {
                ObtemRelatorios();
            }
        }

        private void BtnExcluir_Click(object sender, RoutedEventArgs e) {
            if ((bool)rdExcluirRpt.IsChecked) {
                ExcluiArquivos();
            }
            if ((bool)rdExcluirModulos.IsChecked) {
                ExcluiModulos();
            }
            if ((bool)rdExcluirModulosRpt.IsChecked) {
                ExcluiModulos();
                ObtemRelatorios();
                ExcluiArquivos();
            }
        }

        private void RdExcluirModulosRpt_Checked(object sender, RoutedEventArgs e) {

        }
    }
}
