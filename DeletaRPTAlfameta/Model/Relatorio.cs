﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeletaRPTAlfameta.Model {
    class Relatorio {
        public String CodRelatorio { get; set; }
        public String Titulo { get; set; }
        public String Arquivo { get; set; }
        public String CodMenuProcesso { get; set; }
        public String TituloMenuProcesso { get; set; }
        public String CodModulo { get; set; }
        public String NomeModulo { get; set; }
        public String ModuloAtivo { get; set; }

        public Relatorio(String CodRelatorio, String Titulo, String Arquivo, String CodMenuProcesso, String TituloMenuProcesso, String CodModulo, String NomeModulo, String ModuloAtivo) {
            this.CodRelatorio = CodRelatorio;
            this.Titulo = Titulo;
            this.Arquivo = Arquivo;
            this.CodMenuProcesso = CodMenuProcesso;
            this.TituloMenuProcesso = TituloMenuProcesso;
            this.CodModulo = CodModulo;
            this.NomeModulo = NomeModulo;
            this.ModuloAtivo = ModuloAtivo;
        }

    }
}
