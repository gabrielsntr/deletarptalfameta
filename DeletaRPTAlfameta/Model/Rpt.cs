﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeletaRPTAlfameta.Model {
    class Rpt {
        public String Nome { get; set; }
        public String Tipo { get; set; }
        public String Caminho { get; set; }
        public String Cadastrado { get; set; }


        public Rpt(String nome, String tipo, String caminho, String cadastrado) {
            this.Nome = nome;
            this.Tipo = tipo;
            this.Caminho = caminho;
            this.Cadastrado = cadastrado;
        }

    }
}
