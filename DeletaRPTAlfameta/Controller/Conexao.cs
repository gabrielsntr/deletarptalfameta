﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace DeletaRPTAlfameta.Controller {

    class Conexao {

         
        

        public static MySqlConnection Conectar(String schema) {
            String connStringInformationSchema = "Server=localhost;Database=" + schema + ";Uid=root;Pwd=";
            MySqlConnection connection = new MySqlConnection(connStringInformationSchema);
            return connection;
        }

        public void Desconectar(MySqlConnection conexao) {
            conexao.Clone();
        }

        public static MySqlDataReader RetornaDataReader(String schema, String sql) {
            MySqlConnection conexao = Conectar(schema);
            conexao.Open();
            MySqlCommand comando = new MySqlCommand(sql, conexao);
            MySqlDataReader reader = comando.ExecuteReader();
            return reader;
        }

        public static int ExecutaSQL(String schema, String sql) {
            MySqlConnection conexao = Conectar(schema);
            conexao.Open();
            MySqlCommand comando = new MySqlCommand(sql, conexao);
            int rows = comando.ExecuteNonQuery();
            conexao.Close();
            return rows;
        }
        
        public static List<String> ListaModulosInativos(String schema) {
            MySqlDataReader reader = RetornaDataReader(schema, "select coalesce(Nome, '') as Nome from modulo where Ativo = 'N';");
            List<String> result = new List<String>();
            int i = 0;
            while (reader.Read()) {
                result.Add(reader.GetString(0));
            }
            reader.Close();
            return result;
        }

        public static int DeletaModulosInativos(String schema) {
            String sql = "delete from " + schema + ".acessomenu where CodModulo IN (SELECT CodModulo FROM " + schema + ".modulo WHERE Ativo = 'N'); " +
                         "delete from " + schema + ".acessomodulo where CodModulo IN (SELECT CodModulo FROM " + schema + ".modulo WHERE Ativo = 'N'); " +
                          "delete from " + schema + ".acessomoduloxempresa where CodModulo IN (SELECT CodModulo FROM " + schema + ".modulo WHERE Ativo = 'N'); " +
                          "delete from " + schema + ".ajuda where CodModulo IN (SELECT CodModulo FROM " + schema + ".modulo WHERE Ativo = 'N'); " +
                          "delete from " + schema + ".menu where CodModulo IN (SELECT CodModulo FROM " + schema + ".modulo WHERE Ativo = 'N'); " +
                          "delete from " + schema + ".menufavorito where CodModulo IN (SELECT CodModulo FROM " + schema + ".modulo WHERE Ativo = 'N'); " +
                          "delete from " + schema + ".padraoempresaxmodulo where CodModulo IN (SELECT CodModulo FROM " + schema + ".modulo WHERE Ativo = 'N'); " +
                          "delete FROM " + schema + ".ultimaoperacao where CodModulo IN (SELECT CodModulo FROM " + schema + ".modulo WHERE Ativo = 'N'); " +
                          "delete from " + schema + ".modulo where Ativo = 'N';";
            return ExecutaSQL(schema, sql);
        }
    }
}
