﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeletaRPTAlfameta.Controller
{
    class Schemas
    {

        public static List<String> getSchemas() {
            List<String> schemas = new List<String>();

            String sql = "select SCHEMA_NAME from information_schema.SCHEMATA WHERE SCHEMA_NAME like ('%dicionario%') order by SCHEMA_NAME";

            MySqlDataReader dados = Conexao.RetornaDataReader("information_schema", sql);
            while (dados.Read()) {
                schemas.Add(dados[0].ToString());
            }
            dados.Close();
            return schemas;
    }

        



    }
}
