﻿using DeletaRPTAlfameta.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeletaRPTAlfameta.Controller {
    class Relatorios {
        public static List<Relatorio> GetRelatorios(String schema) {

            List<Relatorio> relatorios = new List<Relatorio>();
            String sql = "select coalesce(r.CodRelatorio,'') as CodRelatorio, " +
                            "   coalesce(r.Titulo,'') as Titulo, " +
                            "	coalesce(r.Arquivo,'') as Arquivo, " +
                            "	coalesce(m.CodMenu,'') AS CodMenuProcesso, " +
                            "	coalesce(m.Titulo,'') AS TituloMenuProcesso, " +
                            "	coalesce(m.CodModulo,'') as CodModulo, " +
                            "	coalesce(md.Nome,'') AS NomeModulo, " +
                            "	coalesce(md.Ativo,'') AS ModuloAtivo from " +
                            "	relatorio AS r JOIN " +
                            "	menu AS m ON(r.CodRelatorio = m.ValorAcao) JOIN " +
                            "	modulo AS md ON(m.CodModulo = md.CodModulo) " +
                            "UNION DISTINCT " +
                            "select coalesce(r.CodRelatorio,'') as CodRelatorio, " +
                            "	coalesce(r.Titulo,'') as Titulo, " +
                            "	coalesce(r.Arquivo,'') as Arquivo, " +
                            "	coalesce(m.CodProcessoTabela,'') AS CodMenuProcesso, " +
                            "	coalesce(m.Titulo,'') AS TituloMenu, " +
                            "	0 AS CodModulo, " +
                            "	'Processo de Tabela' AS NomeModulo, " +
                            "	'S' AS ModuloAtivo from " +
                            "	relatorio AS r JOIN " +
                            "	processotabela AS m ON(r.CodRelatorio = m.ValorAcao) " +
                            "UNION DISTINCT " +
                            "select coalesce(r.CodRelatorio,'') as CodRelatorio, " +
                            "	coalesce(r.Titulo,'') as Titulo, " +
                            "	coalesce(r.ArquivoLayout,'') as Arquivo, " +
                            "	coalesce(m.CodMenu,'') AS CodMenuProcesso, " +
                            "	coalesce(m.Titulo,'') AS TituloMenuProcesso, " +
                            "	coalesce(m.CodModulo,'') as CodModulo, " +
                            "	coalesce(md.Nome,'') AS NomeModulo, " +
                            "	coalesce(md.Ativo,'') AS ModuloAtivo from " +
                            "	layoutrelatorio AS r JOIN " +
                            "	menu AS m ON(r.CodRelatorio = m.ValorAcao) JOIN " +
                            "	modulo AS md ON(m.CodModulo = md.CodModulo) " +
                            "UNION DISTINCT " +
                            "select coalesce(r.CodRelatorio,'') as CodRelatorio, " +
                            "	coalesce(r.Titulo,'') as Titulo, " +
                            "	coalesce(r.ArquivoLayout,'') as Arquivo, " +
                            "	coalesce(m.CodProcessoTabela,'') AS CodMenuProcesso, " +
                            "	coalesce(m.Titulo,'') AS TituloMenu, " +
                            "	0 AS CodModulo, " +
                            "	'Processo de Tabela' AS NomeModulo, " +
                            "	'S' AS ModuloAtivo from " +
                            "	layoutrelatorio AS r JOIN " +
                            "	processotabela AS m ON(r.CodRelatorio = m.ValorAcao) " +
                            "ORDER BY CodModulo, CodMenuProcesso, Arquivo;";

            MySqlDataReader dados = Conexao.RetornaDataReader(schema, sql);
            while (dados.Read()) {
                Relatorio r = new Relatorio(dados.GetString(0), dados.GetString(1), dados.GetString(2), dados.GetString(3), dados.GetString(4), dados.GetString(5), dados.GetString(6), dados.GetString(7));
                relatorios.Add(r);
            }
            dados.Close();

            return relatorios;
        }
        public static void VerificaModuloAtivo(List<Relatorio> relatorios) {
            List<Relatorio> relatoriosDeModulosAtivos = new List<Relatorio>();
            foreach(Relatorio r in relatorios) {
                if (r.ModuloAtivo.Equals("S") && !string.IsNullOrWhiteSpace(r.Arquivo)) {
                    relatoriosDeModulosAtivos.Add(r);
                }
            }
            int i = 0;
            foreach (Relatorio relatorio in relatorios) {
                if (!string.IsNullOrWhiteSpace(relatorio.Arquivo)) {
                    foreach (Relatorio relatorioAtivo in relatoriosDeModulosAtivos) {
                        if (relatorio.Arquivo.Equals(relatorioAtivo.Arquivo)) {
                            relatorios[i].ModuloAtivo = "S";
                            break;
                        }
                    }
                }
                i++;
            }
        }
    }
}
