﻿using DeletaRPTAlfameta.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeletaRPTAlfameta.Controller {
    class Rpts {

        public static List<Rpt> GetRpts(String pasta) {
            if (string.IsNullOrWhiteSpace(pasta)) {
                throw new NullReferenceException();
            } else {
                String[] arquivos = Directory.GetFiles(pasta, "*.rpt", SearchOption.AllDirectories);
                List<Rpt> rpts = new List<Rpt>();
                foreach (String arquivo in arquivos) {
                    String[] dir = arquivo.Split('\\');
                    String tipo;
                    if (dir[dir.Length - 2] == "Customizado") {
                        tipo = "Customizado";
                    } else if (dir[dir.Length - 2] == "old") {
                        tipo = "Old";
                    } else {
                        tipo = "Padrão";
                    }
                    rpts.Add(new Rpt(arquivo.Split('\\')[dir.Length - 1], tipo, arquivo, "N"));
                }

                return rpts;
            }

        }
        public static void SetCadastrado(List<Rpt> arquivos, List<Relatorio> relatorios) {
            String[] nomeRelatorios = new String[relatorios.Count];
            int i = 0;
            foreach (Relatorio relatorio in relatorios) {
                nomeRelatorios[i] = relatorio.Arquivo;
                i++;
            }

            for (int j = 0; j < arquivos.Count; j++) {
                Rpt arquivo = arquivos[j];
                foreach (String nome in nomeRelatorios) {
                    if (arquivo.Nome.Equals(nome)) {
                        arquivos[j].Cadastrado = "S";
                    }
                }
            }
        }

        public static List<Rpt> ListaRptExclusao(List<Rpt> arquivos) {
            List<Rpt> exclusao = new List<Rpt>();
            foreach (Rpt arquivo in arquivos) {
                if (arquivo.Cadastrado == "N") {
                    exclusao.Add(arquivo);
                }
            }
            return exclusao;
        }
        public static int[] ExcluiRpt(List<Rpt> arquivosExcluir) {
            int[] result = { 0, 0 };
            foreach (Rpt exc in arquivosExcluir) {
                try {
                    File.Delete(exc.Caminho);
                    result[0] += 1;
                } catch (Exception ex) {
                    result[1] += 1; 
                }
            }
            return result;
        }

    }
}
